const products = [
  {
    id: "1",
    thumbnail: "/assets/products/red-seat.jpg",
    name: "Red Seat",
    description:
      "This seat is nicely and will fit perfectly in your living room. Lorem ipsum sit dolor amet. Consecteture adipscing elit. Utfermentum interdum malesuada. Sed ornare posuere lobortis. Lasis Utfermentum interdum malesuada sed ornare.",
    price: 45.0,
    rating: 4.5,
  },
  {
    id: "2",
    thumbnail: "/assets/products/modern-table.jpg",
    name: "White Table",
    description: "Lorem ipsum sit dolor amet",
    price: 350.49,
    importance: 1,
    rating: 4.2,
  },
  {
    id: "3",
    thumbnail: "/assets/products/blue-seat.jpg",
    name: "Blue Seat",
    description: "Lorem ipsum sit dolor amet",
    price: 35.0,
    rating: 4,
  },
  {
    id: "4",
    thumbnail: "/assets/products/modern-bed.jpg",
    name: "Modern Bed",
    description: "Lorem ipsum sit dolor amet",
    price: 120.0,
    importance: 2,
    rating: 4,
  },
  {
    id: "5",
    thumbnail: "/assets/products/dark-seat.jpg",
    name: "Dark Seat",
    description: "Lorem ipsum sit dolor amet",
    price: 50.6,
    rating: 2.5,
  },
  {
    id: "6",
    thumbnail: "/assets/products/house1.jpg",
    name: "TV FURNITURE",
    description: "Lorem ipsum sit dolor amet",
    price: 540.6,
    importance: 2,
    rating: 3.5,
  },
  {
    id: "7",
    thumbnail: "/assets/products/house2.jpg",
    name: "Carbinet",
    description: "Lorem ipsum sit dolor amet",
    price: 430.6,
    importance: 2,
    rating: 3.8,
  },
];

function getProduct(id) {
  return products.filter((product) => product.id === id);
}

function getProducts(sort = 0) {
  const sortedProducts = [...products];
  if (sort === 0) return sortedProducts;
  if (sort === 1) return sortedProducts;
  if (sort === 2) return sortedProducts;
  if (sort === 3) {
    sortedProducts.map((product) => (product.importance = null));
    sortedProducts.sort((a, b) => a.price - b.price);
  }
  if (sort === 4) return sortedProducts;
  return sortedProducts;
}

export { getProducts, getProduct };
