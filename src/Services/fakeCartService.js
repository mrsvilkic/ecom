let cart = {
  content: [],
  priceOfCart: 0,
  itemsInCart: 0,
};

const addToCart = (item) => {
  cart.content.push(item);
  calculateValue();
  itemCount();
};
const deleteFromCart = (item) => {
  cart.content = cart.content.filter((i) => i.id !== item.id);
  calculateValue();
  itemCount();
};
const getCart = () => {
  return cart;
};
const itemCount = () => {
  cart.itemsInCart = cart.content.length;
};
const calculateValue = () => {
  cart.priceOfCart = cart.content.reduce((p, c) => p + c, 0);
};

export { addToCart, deleteFromCart, getCart };
