import React, { useEffect, useState, useContext } from "react";
import Dropdown from "../Components/Common/Dropdown";
import NumberInput from "../Components/Common/NumberInput";
import CartContext from "../Context/CartContext";
import { getProduct } from "../Services/fakeProductsService";

const options = [
  { id: 0, name: "Red", active: true },
  { id: 1, name: "Green" },
  { id: 2, name: "Blue" },
  { id: 3, name: "Brown" },
];

const Product = ({ match }) => {
  const [product, setProduct] = useState({});
  const [quantity, setQuantity] = useState(1);
  const [color, setColor] = useState(options[0]);
  const { onAdd } = useContext(CartContext);

  useEffect(() => {
    setProduct(getProduct(match.params.id)[0]);
  }, []);

  return (
    <div className="product ">
      <div className="product-preview level-item">
        <img src={product.thumbnail} alt="red-seat" />
      </div>
      <div className="product-info">
        <div className="category-title">
          <h2>House</h2>
        </div>
        <div className="product-body">
          <h3 className="product-name">
            {product.name}
            <span>SKU-887</span>
          </h3>

          <p className="product-description">{product.description}</p>
          <div className="product-options">
            <div className="product-option is-fullwidth-mobile">
              <div className="option-name">PRICE</div>
              <div className="product-price">${product.price}</div>
            </div>
            <div className="product-option is-fullwidth-mobile">
              <div className="option-name">SIZES</div>
              <Dropdown
                values={options}
                onSelect={(color) => setColor(color)}
              />
            </div>
            <div className="product-option is-fullwidth-mobile">
              <div className="option-name">QUANTITY</div>
              <NumberInput value={quantity} onChange={setQuantity} min={1} />
            </div>
            <div className="product-option is-align-self-flex-end is-fullwidth-mobile">
              <button
                className="btn-base button is-fullwidth-mobile"
                onClick={() => onAdd(product)}
              >
                Add to cart
              </button>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Product;
