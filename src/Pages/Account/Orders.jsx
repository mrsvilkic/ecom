import React from "react";
import GuestAccount from "../../Components/Common/Account/GuestAccount";

function Orders(props) {
  return (
    <GuestAccount
      img="/assets/orders.svg"
      text="Guest users don't have accounts. Please login or register to view your account."
    />
  );
}

export default Orders;
