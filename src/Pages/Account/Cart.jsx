import React, { useContext } from "react";
import CartProduct from "../../Components/Common/Cart/CartProduct";
import CartContext from "../../Context/CartContext";
import CartInfo from "../../Components/Common/Cart/CartInfo";
import CartEmpty from "../../Components/Common/Cart/CartEmpty";

function Cart(props) {
  const { cart, onDelete } = useContext(CartContext);

  if (!cart.itemsInCart) return <CartEmpty />;
  return (
    <div className="columns mt-5">
      <div className="column is-8">
        {cart.content &&
          cart.content.map((product) => (
            <CartProduct key={product.id} item={product} onDelete={onDelete} />
          ))}
      </div>
      <div className="column is-4">
        <CartInfo cart={cart} />
      </div>
    </div>
  );
}

export default Cart;
