import React from "react";
import GuestAccount from "./../../Components/Common/Account/GuestAccount";

function Account(props) {
  return (
    <GuestAccount
      img="/assets/guest.svg"
      text="Guest users don't have accounts. Please login or register to view your account."
    />
  );
}

export default Account;
