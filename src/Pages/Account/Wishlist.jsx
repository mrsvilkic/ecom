import React from "react";
import Wish from "./../../Components/Common/Wishlist/Wish";

const products = [
  {
    id: "2",
    thumbnail: "/assets/products/modern-table.jpg",
    name: "White Table",
    description: "Lorem ipsum sit dolor amet",
    price: 350.49,
    importance: 1,
    rating: 4.2,
  },
  {
    id: "3",
    thumbnail: "/assets/products/blue-seat.jpg",
    name: "Blue Seat",
    description: "Lorem ipsum sit dolor amet",
    price: 35.0,
    rating: 4,
  },
  {
    id: "4",
    thumbnail: "/assets/products/modern-bed.jpg",
    name: "Modern Bed",
    description: "Lorem ipsum sit dolor amet",
    price: 120.0,
    importance: 2,
    rating: 4,
  },
];

function Wishlist(props) {
  // return (
  //   <GuestAccount
  //     img="/assets/wishlist.svg"
  //     text="Please login to your account, or create a new one to be able to use wishlists."
  //   />
  // );
  return (
    <div className="columns mt-5">
      <div className="column is-12">
        {products.map((product) => (
          <Wish key={product.id} item={product} />
        ))}
      </div>
    </div>
  );
}

export default Wishlist;
