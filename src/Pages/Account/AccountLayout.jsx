import { Route } from "react-router-dom";
import AccountTabs from "./../../Components/Common/Account/AccountTabs";
import Account from "./Account";
import Cart from "./Cart";
import Orders from "./Orders";
import Wishlist from "./Wishlist";

const AccountLayout = ({ match }) => {
  return (
    <section className="section">
      <div className="section-header mb-5">
        <h2>{match.path.substring(1)}</h2>
      </div>
      <AccountTabs />
      <Route path="/account" component={Account} />
      <Route path="/wishlist" component={Wishlist} />
      <Route path="/cart" component={Cart} />
      <Route path="/orders" component={Orders} />
    </section>
  );
};

export default AccountLayout;
