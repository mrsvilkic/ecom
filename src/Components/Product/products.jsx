import React, { useEffect, useState } from "react";
import ProductCard from "./productCard";
import { getProducts } from "../../Services/fakeProductsService";
import { IoGridOutline, IoListOutline } from "react-icons/io5";
import Dropdown from "../Common/Dropdown";

const sortOptions = [
  { id: 0, name: "Default Sort", active: true },
  { id: 1, name: "Best Seller" },
  { id: 2, name: "Highest Rated" },
  { id: 3, name: "Best Deals" },
  { id: 4, name: "Newest Products" },
];

function Products() {
  const [products, setProducts] = useState([]);

  useEffect(() => {
    setProducts(getProducts());
  }, []);

  const handleSort = (option) => {
    setProducts(getProducts(option.id));
  };

  return (
    <section className="section">
      <div className="section-header">
        <h2>House {}</h2>
      </div>
      <div className="is-flex level my-3">
        <div className="is-flex columns.is-vcentered">
          <IoGridOutline size="1.4em" className="is-icon mr-3 is-active" />
          <IoListOutline size="1.4em" className="is-icon" />
        </div>
        <div>
          <Dropdown values={sortOptions} onSelect={handleSort} />
        </div>
      </div>
      <div className="columns is-multiline">
        {products.map((product) => (
          <ProductCard key={product.id} product={product} />
        ))}
      </div>
    </section>
  );
}

export default Products;
