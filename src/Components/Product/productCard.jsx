import React, { useContext } from "react";
import { FiShoppingCart } from "react-icons/fi";
import { NavLink } from "react-router-dom";
import CartContext from "../../Context/CartContext";

function ProductCard({ product }) {
  const { onAdd } = useContext(CartContext);
  const { name, description, price, thumbnail, importance } = product;

  const getDisplayClass = (importance) => {
    return importance
      ? importance === 1
        ? "column is-9 "
        : "column is-6 "
      : "column is-3 ";
  };

  const getIsLong = (importance) => {
    return importance ? "flat-card is-long" : "flat-card";
  };

  return (
    <div className={getDisplayClass(importance)}>
      <div className={getIsLong(importance)}>
        <div className="image">
          <img src={thumbnail} alt={name} />
        </div>

        <div className="product-info">
          <NavLink
            className="product-details-link"
            to={`/product/${product.id}`}
          >
            <h3 className="product-name">{name}</h3>
          </NavLink>
          <p className="product-description">{description.slice(0, 40)}...</p>
          <p className="product-price">${price}</p>
          <div className="actions">
            <FiShoppingCart onClick={() => onAdd(product)} />
          </div>
        </div>
      </div>
    </div>
  );
}

export default ProductCard;
