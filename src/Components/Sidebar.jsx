import React, { useState, useContext } from "react";
import { FiShoppingCart, FiHeart, FiHome, FiSearch } from "react-icons/fi";
import { IoIosArrowForward, IoIosMenu } from "react-icons/io";
import { NavLink } from "react-router-dom";
import CartContext from "./../Context/CartContext";
import SideLink from "./Common/SideLink";

function Sidebar(props) {
  const [slided, setSlide] = useState(false);
  const { cart } = useContext(CartContext);

  const hideSlide = () => {
    setSlide(false);
  };

  const getSlideClass = () => {
    return slided ? "menu side-popup slide" : " menu side-popup retreat";
  };

  const navLinks = [
    {
      name: "Home",
      element: (
        <FiHome
          className="is-icon"
          size="1.2em"
          onClick={() => {
            setSlide(!slided);
          }}
        />
      ),
    },
    {
      name: "Wishlist",
      element: (
        <NavLink className=" is-relative is-muted" to="/wishlist">
          <FiHeart className="is-icon" size="1.2em" onClick={hideSlide} />
          {/* {cart.itemsInCart ? <span className="dot-alert" /> : ""} */}
        </NavLink>
      ),
    },
    {
      name: "Cart",
      element: (
        <NavLink className=" is-relative is-muted" to="/cart">
          <FiShoppingCart
            className="is-icon"
            size="1.2em"
            onClick={hideSlide}
          />
          {cart.itemsInCart ? <span className="dot-alert" /> : ""}
        </NavLink>
      ),
    },
    {
      name: "Search",
      element: (
        <FiSearch
          className="is-icon "
          size="1.2em"
          onClick={() => {
            setSlide(!slided);
          }}
        />
      ),
    },
  ];

  return (
    <div className="side">
      <aside className="main-sidebar">
        <div className="main-brand">
          <img src="/assets/logo/nephos.svg" alt="brand-logo" />
        </div>
        <ul className="side-list">
          {navLinks.map((link) => (
            <li className="side-link is-hidden-mobile" key={link.name}>
              {link.element}
            </li>
          ))}
          <li className="side-link is-hidden-desktop is-hidden-tablet is-block-mobile">
            <IoIosMenu
              className="is-icon"
              size="1.5em"
              onClick={() => {
                setSlide(!slided);
              }}
            />
          </li>
        </ul>
      </aside>

      {/* Side menu popup */}
      <div className={getSlideClass()}>
        <div className="has-text-right ">
          <IoIosArrowForward
            onClick={hideSlide}
            className="m-3 is-clickable is-hoverable hover-rotate"
            size="1.5em"
          />
        </div>
        <p className="menu-label no-select">General</p>
        <ul className="menu-list">
          <SideLink path="/" text="Home" func={hideSlide} />
          <SideLink path="/shop" text="Shop" func={hideSlide} />
          <SideLink path="/products" text="All products" func={hideSlide} />
        </ul>
        <p className="menu-label no-select">Administration</p>
        <ul className="menu-list">
          <SideLink path="/account" text="My Account" func={hideSlide} />
          <SideLink
            path="/cart"
            text="My Cart"
            func={hideSlide}
            className="is-hidden-tablet is-hidden-desktop is-block-mobile"
          />
          <SideLink path="/wishlist" text="My Wishlist" func={hideSlide} />
          <SideLink path="/orders" text="My Orders" func={hideSlide} />
        </ul>
      </div>
    </div>
  );
}

export default Sidebar;
