import React from "react";
import { NavLink } from "react-router-dom";

function GuestAccount({ img, text }) {
  return (
    <div className="columns mt-5">
      <div className="column has-text-centered">
        <img className="w-200" src={img} alt="Guest" />
        <h3 className="is-size-5 my-3">Welcome Guest</h3>
        <div className="column column is-4 m-auto">
          <p className="color-muted mb-4">{text}</p>
        </div>
        <NavLink to="/products" className="button btn-base w-200">
          Log in
        </NavLink>
      </div>
    </div>
  );
}

export default GuestAccount;
