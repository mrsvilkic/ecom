import React from "react";
import { NavLink } from "react-router-dom";

function AccountTabs(props) {
  return (
    <div className="tabs account-tabs my-3">
      <ul>
        <li>
          <NavLink to="/account" activeClassName="tab-active">
            Account
          </NavLink>
        </li>
        <li>
          <NavLink to="/wishlist" activeClassName="tab-active">
            Wishlist
          </NavLink>
        </li>
        <li>
          <NavLink to="/cart" activeClassName="tab-active">
            Cart
          </NavLink>
        </li>
        <li>
          <NavLink to="/orders" activeClassName="tab-active">
            Orders
          </NavLink>
        </li>
      </ul>
    </div>
  );
}

export default AccountTabs;
