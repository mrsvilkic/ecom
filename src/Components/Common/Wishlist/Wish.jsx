import React from "react";
import { IoTrashOutline } from "react-icons/io5";
import { FiShoppingCart } from "react-icons/fi";
import { NavLink } from "react-router-dom";
function Wish({ item, onDelete }) {
  return (
    <div className="wish-product">
      <div className="image">
        <img src={item.thumbnail} alt="red-seat" />
      </div>
      <div className="column is-flex is-align-items-center">
        <div className="column is-8">
          <span className="has-text-weight-semibold">
            <NavLink to={`/product/${item.id}`}>{item.name}</NavLink>
          </span>
          <p>House</p>
        </div>
        <div className="column has-text-right">
          <FiShoppingCart className="is-muted is-icon m-4 " />
          <IoTrashOutline
            className="is-muted is-icon m-4 "
            onClick={() => {
              onDelete(item);
            }}
          />
        </div>
      </div>
    </div>
  );
}

export default Wish;
