import React from "react";

function Footer(props) {
  return (
    <footer style={{ position: "fixed", bottom: 0, left: "45%" }}>
      @2021 - Aleksa Svilkic - <b>mrsvilkic@gmail.com</b>
    </footer>
  );
}

export default Footer;
