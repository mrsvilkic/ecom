import React from "react";
import { NavLink } from "react-router-dom";

function SideLink({ path, text, func, className = "" }) {
  return (
    <li>
      <NavLink className={className} to={path} onClick={func}>
        {text}
      </NavLink>
    </li>
  );
}

export default SideLink;
