import React, { useEffect, useState } from "react";
import { IoCaretDown } from "react-icons/io5";

function Dropdown({ values, onSelect }) {
  const [sort, setSort] = useState({});
  const [isDropdown, setDropdown] = useState(false);
  const [options, setOptions] = useState([]);

  useEffect(() => {
    setSort(values[0]);
    setOptions(values);
  }, []);

  const handleSort = (e) => {
    const newOptions = [...options];
    newOptions.map((o) =>
      o.name === e.target.innerHTML ? (o.active = true) : (o.active = false)
    );

    const sort = newOptions.filter((o) => o.name === e.target.innerHTML)[0];
    setDropdown(false);
    setSort(sort);
    setOptions(newOptions);
    onSelect(sort);
  };

  return (
    <div className="dropdown is-right is-width-inherit">
      <div className="dropdown-trigger is-width-inherit">
        <button
          className="button is-fullwidth-mobile is-muted is-drop-button"
          onClick={() => setDropdown(!isDropdown)}
          style={{ minWidth: "130px" }}
        >
          <span>{sort.name}</span>
          <span className="icon is-small">
            <IoCaretDown />
          </span>
        </button>
      </div>
      <div
        className={`dropdown-menu is-block is-fullwidth-mobile ${
          isDropdown ? "" : "is-invisible"
        }`}
      >
        <div className="dropdown-content ">
          {options.map((option) => (
            <div
              key={option.name}
              className={
                option.active === true
                  ? "dropdown-item is-icon is-active"
                  : "dropdown-item is-icon"
              }
              onClick={(e) => handleSort(e)}
            >
              {option.name}
            </div>
          ))}
        </div>
      </div>
    </div>
  );
}

export default Dropdown;
