import React, { useState } from "react";
import NumberInput from "../NumberInput";
import { IoTrashOutline } from "react-icons/io5";

function CartProduct({ item, onDelete }) {
  const [quantity, setQuantity] = useState(1);
  return (
    <div className="cart-product">
      <div className="image">
        <img src={item.thumbnail} alt="red-seat" />
      </div>
      <div className="column cart-info">
        <div className="column is-5">
          <span className="has-text-weight-semibold">{item.name}</span>
          <p>House</p>
        </div>
        <div className="column has-text-left-tablet has-text-centered-mobile">
          <p className="is-uppercase is-size-7">Price</p>
          <p className="is-size-5-tablet is-size-6-mobile has-text-weight-medium">
            ${item.price}
          </p>
        </div>
        <div className="column">
          <NumberInput
            value={quantity}
            min={1}
            onChange={setQuantity}
            className="number-input-border"
          />
          <IoTrashOutline
            className="is-icon"
            onClick={() => {
              onDelete(item);
            }}
          />
        </div>
      </div>
    </div>
  );
}

export default CartProduct;
