import React from "react";
import { FiShoppingCart } from "react-icons/fi";

function CartInfo({ cart }) {
  return (
    <div className="cart-order-summary">
      <div className="is-flex">
        <p className="is-flex is-align-items-center mb-3">
          <FiShoppingCart className="mr-2" />
          <b className="mr-2">{cart.itemsInCart}</b> Item(s) in Cart
        </p>
      </div>
      <div className="summary-item mute-color">
        <div>Subtotal</div> <div>${Number(cart.priceOfCart).toFixed(2)}</div>
      </div>
      <div className="summary-item mute-color">
        <div>Taxes</div> <div>$0.00</div>
      </div>
      <div className="summary-item mute-color">
        <div>Shipping</div> <div>$0.0</div>
      </div>
      <div className="summary-item has-text-weight-bold is-size-5 mb-4 ">
        <div>Total</div> <div>${Number(cart.priceOfCart).toFixed(2)}</div>
      </div>
      <button className="button btn-base">Checkout</button>
    </div>
  );
}

export default CartInfo;
