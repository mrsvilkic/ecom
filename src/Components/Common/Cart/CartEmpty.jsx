import React from "react";
import { NavLink } from "react-router-dom";

function CartEmpty(props) {
  return (
    <div className="columns mt-5">
      <div className="column has-text-centered">
        <img
          src="assets/empty-cart.svg"
          alt="Empty Cart"
          style={{ width: "200px" }}
        />
        <h3 className="is-size-5">Empty Cart</h3>
        <div className="column column is-4 m-auto">
          <p className="color-muted mb-4">
            Your shopping cart is currenlty empty. Start adding products to be
            able to checkout.
          </p>
        </div>
        <NavLink to="/products" className="button btn-base">
          Continue Shopping
        </NavLink>
      </div>
    </div>
  );
}

export default CartEmpty;
