import React from "react";

function NumberInput({ value, onChange, className, min = 0, max = 999 }) {
  const incrementValue = () => {
    onChange(value + 1);
  };
  const decrementValue = () => {
    if (value - 1 >= min) onChange(value - 1);
  };

  return (
    <div className={className + " number-input"}>
      <button
        className="button is-radiusless borderless is-icon is-muted left-radius has-text-weight-bold has-shadow-left"
        onClick={decrementValue}
      >
        ‒
      </button>
      <span
        className="button is-muted is-radiusless borderless "
        style={{ padding: 0, zIndex: 2 }}
      >
        {value}
      </span>
      <button
        className="button is-radiusless borderless is-icon is-muted right-radius has-text-weight-bold has-shadow-right"
        onClick={incrementValue}
      >
        +
      </button>
    </div>
  );
}

export default NumberInput;
