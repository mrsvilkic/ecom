import { useState, useEffect } from "react";
import { Redirect, Route, Switch } from "react-router-dom";

import Products from "./Components/Product/products";
import Sidebar from "./Components/Sidebar";
import Product from "./Pages/product";
import CartContext from "./Context/CartContext";
import { getCart, deleteFromCart, addToCart } from "./Services/fakeCartService";
import AccountLayout from "./Pages/Account/AccountLayout";
import Footer from "./Components/Common/Footer";

function App() {
  const [cart, setCart] = useState([]);
  const [wishlist, setWishlist] = useState([]);

  const handleCartDelete = (product) => {
    const content = [...cart.content].filter((item) => item.id !== product.id);
    const itemsInCart = content.length;
    const priceOfCart = content.reduce((sum, item) => sum + item.price, 0);
    setCart({ content, itemsInCart, priceOfCart });
    deleteFromCart(product);
  };

  const handleCartAdd = (product) => {
    const content = [product, ...cart.content];
    const itemsInCart = content.length;
    const priceOfCart = content.reduce((sum, item) => sum + item.price, 0);
    setCart({ content, itemsInCart, priceOfCart });
    addToCart(product);
  };

  const handleWishDelete = (product) => {};

  const handleWishtAdd = (product) => {};

  useEffect(() => {
    setCart(getCart());
  }, []);

  return (
    <>
      <CartContext.Provider
        value={{
          cart,
          onDelete: handleCartDelete,
          onAdd: handleCartAdd,
        }}
      >
        <Sidebar />
        <div className="page-wrapper">
          <Switch>
            <Route path="/products" component={Products} />
            <Route path="/product/:id" component={Product} />
            <Route
              path={["/account", "/wishlist", "/cart", "/orders"]}
              component={AccountLayout}
            />
            <Redirect from="/" to="/products" />
          </Switch>
        </div>
        <Footer />
      </CartContext.Provider>
    </>
  );
}

export default App;
